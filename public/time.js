// set the date
const lebaranDate = new Date("May 2, 2022 17:45:55").getTime();
const puasaDate = new Date("April 3, 2022 17:55:55").getTime();
const contractDate = new Date("January 31, 2022 23:59:59").getTime();

const distanceTime = (distance) => {
  const days = Math.floor(distance / (1000 * 60 * 60 * 24));
  const hours = Math.floor(
    (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  );
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((distance % (1000 * 60)) / 1000);

  return [days, hours, minutes, seconds];
};

// update the count down every 1 second
const intervalTime = setInterval(() => {
  // get today's date and time
  const now = new Date().getTime();

  // find distance between now and the count date
  const lebaranDdistance = lebaranDate - now;
  const puasaDistance = puasaDate - now;
  const contractDistance = contractDate - now;

  // time calculations for  month, days, hours, minutes, and seconds
  const [pdays, phours, pminutes] = distanceTime(puasaDistance);
  const [ldays, lhours, lminutes, lseconds] = distanceTime(lebaranDdistance);
  const [cdays] = distanceTime(contractDistance);

  // display the result in elment
  document.getElementById("lebaran").innerHTML = `${ldays} days 
    ${lhours} hours ${lminutes} minutes ${lseconds} seconds`;
  document.getElementById("puasa").innerHTML = `${pdays} days 
    ${phours} hours ${pminutes} minutes`;
  document.getElementById("contract").innerHTML = `You have ${cdays} days left`

  // if count down is finished
  if (distance < 0) {
    clearInterval();
    document.getElementById("lebaran").innerHTML = "DONE! Evaluated!";
  }
}, 1000);
